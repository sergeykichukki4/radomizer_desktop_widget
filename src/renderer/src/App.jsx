import TopBar from "./components/TopBar";
import RandomNumberGenerator from "./components/RandomNumberGenerator";
import {useEffect, useState} from "react";

function App() {
  const [isOverlay, setIsOverlay] = useState(false);

  useEffect(() => {
    window.electron.ipcRenderer.on("overlay-mode", () => {
      setIsOverlay((prev) => !prev);
    });

    return () => {
      window.electron.ipcRenderer.removeAllListeners("overlay-mode")
    }
  }, [])

  return (
    <>
      <div className={!isOverlay ? "visible" : "invisible"}><TopBar/></div>
      <RandomNumberGenerator/>
    </>
  )
}

export default App

