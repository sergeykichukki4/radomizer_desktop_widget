import React from 'react';

const TopBar = () => {
  const handleClose = () => {
    console.log('test close');
    window.electron.ipcRenderer.send("close-window");
  }
  const handleMinimize = () => {
    console.log('test minimize');
    window.electron.ipcRenderer.send("minimize-window");
  }


  return (
    <div style={{position: "relative", textAlign: "center", minWidth: "150px"}}>
      <div className="rounded-t-xl bg-blue-400 w-screen h-7 text-stone-200" style={{WebkitAppRegion: 'drag'}}>Ctrl+6</div>
      <div id="control-buttons" className="text-stone-200 absolute top-1 right-0 pe-2" style={{ WebkitAppRegion: 'no-drag' }}>
        <button id="minimize" onClick={handleMinimize}>
          &#128469;
        </button>
        <button id="close" onClick={handleClose}>
          &#x2715;
        </button>
      </div>
    </div>
  );
};

export default TopBar;
