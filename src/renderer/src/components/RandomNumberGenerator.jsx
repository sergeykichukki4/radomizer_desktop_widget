import React, { useState, useEffect } from 'react';

const RandomNumberGenerator = () => {
  const [number, setNumber] = useState(1);

  useEffect(() => {
    const interval = setInterval(() => {
      setNumber(Math.floor(Math.random() * 100) + 1);
    }, 2000);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className="flex justify-center bg-black bg-opacity-10 rounded-full">
      <h1 className="text-green-500 text-6xl" style={{userSelect: "none", paddingBottom: "5px"}}>{number}</h1>
    </div>
  );
};

export default RandomNumberGenerator;
